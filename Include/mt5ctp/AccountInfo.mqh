//+------------------------------------------------------------------+
//|                                                  AccountInfo.mqh |
//+------------------------------------------------------------------+
#property copyright     "Copyright 2020,Guorui Holding (Shandong) Co.,Ltd."
#property version       "1.50"
#property link          ""
#property description   "本程序仅用于量化投资爱好者测试、研究。程序使用者承诺："
#property description   "1.本人/本单位熟知本程序的适用范围，自愿承担因商业应用造成的违法、违规、侵权责任。"
#property description   "2.本人/本单位熟知金融衍生品的各种交易及代理风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "3.本人/本单位熟知计算机程序的各种固有风险，并自愿承担损失及违法、违规、违约责任。"
#property description   "4.本人/本单位熟知金融监管规则，依法依规参与金融市场，并自愿承担损失及违法、违规责任。"
#property description   "5.本人/本单位认可开始使用本程序，视为同意并做出上述承诺，停用本程序后，上述承诺依然有效。"
//+------------------------------------------------------------------+
//| include 外部依赖                                                 |
//+------------------------------------------------------------------+
#include <Object.mqh>
#include "mt5ctp.mqh"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
class CAccountInfo : public CObject
  {
public:
                     CAccountInfo(void);
                    ~CAccountInfo(void);
   //--- fast access methods to the string account propertyes
   // 登陆账户ID
   string            Login(void) const;
   // 经纪商ID
   string            BrokerID(void) const;
   // 交易日
   string            TradingDay(void) const;
   // 登陆时间
   string            LoginTime(void) const;
   // 币种代码
   string            CurrencyID(void) const;
   // 交易系统名称
   string            SystemName(void) const;
   //--- fast access methods to the integer account propertyes
   // 用户前置
   long              FrontID(void) const;
   // 用户会话
   long              SessionID(void) const;
   // 最大报单引用
   long              MaxOrderRef(void) const;
   // 账户类型
   char              AccountType(void) const;
   // 账户类型注释
   string            AccountTypeDescription(void) const;
   //--- fast access methods to the double account propertyes
   // 昨结算权益
   double            PreBalance(void) const;
   // 昨保证金占用
   double            PreMargin(void) const;
   // 动态权益
   double            Balance(void) const;
   // 可用资金
   double            Available(void) const;
   // 保证金占用
   double            Margin(void) const;
   // 手续费
   double            Commission(void) const;
   // 交割保证金
   double            DeliveryMargin(void) const;
   // 入金
   double            Deposit(void) const;
   // 出金
   double            Withdraw(void) const;
   // 保证金冻结
   double            FrozenMargin(void) const;
   // 手续费冻结
   double            FrozenCommission(void) const;
   // 平仓盈亏
   double            CloseProfit(void) const;
   // 持仓盈亏
   double            PositionProfit(void) const;
   //--- access methods to the API MQL5 functions
   long              InfoInteger(const CTP::ENUM_ACCOUNT_INFO_INTEGER prop_id) const;
   double            InfoDouble(const CTP::ENUM_ACCOUNT_INFO_DOUBLE prop_id) const;
   string            InfoString(const CTP::ENUM_ACCOUNT_INFO_STRING prop_id) const;
   //--- checks
   // 账户是否登陆
   bool              AccountExists(void) const;
  };
//+------------------------------------------------------------------+
//| Constructor                                                      |
//+------------------------------------------------------------------+
CAccountInfo::CAccountInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| Destructor                                                       |
//+------------------------------------------------------------------+
CAccountInfo::~CAccountInfo(void)
  {
  }
//+------------------------------------------------------------------+
//| fast access methods to the string account propertyes             |
//+------------------------------------------------------------------+
// 登陆账户ID
string CAccountInfo::Login(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_AccountID));
  }
// 经纪商ID
string CAccountInfo::BrokerID(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_BrokerID));
  }
// 交易日
string CAccountInfo::TradingDay(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_TradingDay));
  }
// 登陆时间
string CAccountInfo::LoginTime(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_LoginTime));
  }
// 币种代码
string CAccountInfo::CurrencyID(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_CurrencyID));
  }
// 交易系统名称
string CAccountInfo::SystemName(void) const
  {
   return(CTP::AccountInfoString(CTP::ACCOUNT_SystemName));
  }
//+------------------------------------------------------------------+
//| fast access methods to the integer account propertyes            |
//+------------------------------------------------------------------+
// 用户前置
long CAccountInfo::FrontID(void) const
  {
   return(CTP::AccountInfoInteger(CTP::ACCOUNT_FrontID));
  }
// 用户会话
long CAccountInfo::SessionID(void) const
  {
   return(CTP::AccountInfoInteger(CTP::ACCOUNT_SessionID));
  }
// 最大报单引用
long CAccountInfo::MaxOrderRef(void) const
  {
   return(StringToInteger(CTP::AccountInfoString(CTP::ACCOUNT_MaxOrderRef)));
  }
// 账户类型
char CAccountInfo::AccountType(void) const
  {
   return((char)CTP::AccountInfoInteger(CTP::ACCOUNT_BizType));
  }
// 账户类型注释
string CAccountInfo::AccountTypeDescription(void) const
  {
   string str_res;
     {
      char int_res = (char)CTP::AccountInfoInteger(CTP::ACCOUNT_BizType);
      switch(int_res)
        {
         case '1':
            str_res = "THOST_FTDC_BZTP_Future:期货";
            break;
         case '2':
            str_res = "THOST_FTDC_BZTP_Stock:证券";
            break;
         default:
            str_res = "Unknown Account Type";
            break;
        }
     }
   return(str_res);
  }
//+------------------------------------------------------------------+
//| fast access methods to the double account propertyes             |
//+------------------------------------------------------------------+
// 昨结算权益
double CAccountInfo::PreBalance(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_PreBalance));
  }
// 昨保证金占用
double CAccountInfo::PreMargin(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_PreMargin));
  }
// 动态权益
double CAccountInfo::Balance(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_Balance));
  }
// 可用资金
double CAccountInfo::Available(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_Available));
  }
// 保证金占用
double CAccountInfo::Margin(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_CurrMargin));
  }
// 手续费
double CAccountInfo::Commission(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_Commission));
  }
// 交割保证金
double CAccountInfo::DeliveryMargin(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_DeliveryMargin));
  }
// 入金
double CAccountInfo::Deposit(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_Deposit));
  }
// 出金
double CAccountInfo::Withdraw(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_Withdraw));
  }
// 保证金冻结
double CAccountInfo::FrozenMargin(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenMargin));
  }
// 手续费冻结
double CAccountInfo::FrozenCommission(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_FrozenCommission));
  }
// 平仓盈亏
double CAccountInfo::CloseProfit(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_CloseProfit));
  }
// 持仓盈亏
double CAccountInfo::PositionProfit(void) const
  {
   return(CTP::AccountInfoDouble(CTP::ACCOUNT_PositionProfit));
  }
//+------------------------------------------------------------------+
//| Access functions AccountInfoInteger(...)                         |
//+------------------------------------------------------------------+
long CAccountInfo::InfoInteger(const CTP::ENUM_ACCOUNT_INFO_INTEGER prop_id) const
  {
   return(CTP::AccountInfoInteger(prop_id));
  }
//+------------------------------------------------------------------+
//| Access functions AccountInfoDouble(...)                          |
//+------------------------------------------------------------------+
double CAccountInfo::InfoDouble(const CTP::ENUM_ACCOUNT_INFO_DOUBLE prop_id) const
  {
   return(CTP::AccountInfoDouble(prop_id));
  }
//+------------------------------------------------------------------+
//| Access functions AccountInfoString(...)                          |
//+------------------------------------------------------------------+
string CAccountInfo::InfoString(const CTP::ENUM_ACCOUNT_INFO_STRING prop_id) const
  {
   return(CTP::AccountInfoString(prop_id));
  }
//+------------------------------------------------------------------+
//| check account login status                                       |
//| :WRONG_VALUE --未登录                                            |
//| :0   --已经登陆，未确认结算单                                    |
//| :1   --确认结算单，登陆完毕                                      |
//+------------------------------------------------------------------+
// 账户是否登陆
bool CAccountInfo::AccountExists(void) const
  {
   long long_var = WRONG_VALUE;
   if(!mt5ctp::AccountInfoInteger(CTP::ACCOUNT_UserStatus,long_var))
     {
      return false;
     }
   return(long_var>0);
  }
//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
