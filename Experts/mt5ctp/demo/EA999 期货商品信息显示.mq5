//+------------------------------------------------------------------+
//|                                           EA999 期货信号显示.mq5 |
//|                        Copyright 2018, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "EA999.cn 期货商品信息显示"
#property link      "http://www.ea999.cn"
#property description " "
#property description "EA999为您提供服务"
#property description "EA999为您提供EA、指标、脚本的编写或改写服务"
#property description " "
#property description "官网:www.ea999.cn 技术咨询：QQ 547970398"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
//商品信息
#include <mt5ctp\SymbolInfo.mqh>
//帐户信息
#include <mt5ctp\AccountInfo.mqh>

//实例
CSymbolInfo symbol;
CAccountInfo account;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- create timer
   EventSetTimer(1);
// 图表合约类(对象)初始化
   if(!symbol.Select(NULL))
     {
      return(INIT_FAILED);
     }
   //下面代码用于初始化将要显示的标签，内容在Ontick内适时更新
   int OX=10;
   EA999LABEL("EA999100", 0, OX+5, 15,0,"EA999  期货基础信息显示", "微软雅黑", 14,clrGold); //

   EA999LABEL("EA999101", 0, OX+5, 45,0,"商品名称: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999102", 0, OX+5, 60,0,"合约代码: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999103", 0, OX+5, 75,0,"交易所代码: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999104", 0, OX+5, 90,0,"合约在交易所的代码: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999105", 0, OX+5, 105,0,"产品代码: ", "微软雅黑",10,clrLightGreen);
   //EA999LABEL("EA999106", 0, OX+5, 120,0,"产品名称: ", "微软雅黑",10,clrLightGreen);
   //EA999LABEL("EA999107", 0, OX+5, 135,0,"交易所产品代码: ", "微软雅黑",10,clrLightGreen);
   //EA999LABEL("EA999108", 0, OX+5, 150,0,"基础商品代码: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999106", 0, OX+5, 120,0,"是否主力合约: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999107", 0, OX+5, 135,0,"是否订阅行情: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999108", 0, OX+5, 150,0,"是否交易时间: ", "微软雅黑",10,clrLightGreen);   

   EA999LABEL("EA999109", 0, OX+5, 170,0,"多头保证金率: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999110", 0, OX+5, 185,0,"多头保证金费: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999113", 0, OX+5, 235,0,"合约大小: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999114", 0, OX+5, 250,0,"合约商品乘数: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999111", 0, OX+5, 205,0,"多头保证金率: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999112", 0, OX+5, 220,0,"多头保证金费: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999115", 0, OX+5, 275,0,"最大单量限制: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999116", 0, OX+5, 295,0,"最小单量限制: ", "微软雅黑",10,clrLightGreen);
   
   EA999LABEL("EA999117", 0, OX+5, 310,0,"最小价格跳动: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999201", 0, OX+300, 45,0,"到期日: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999202", 0, OX+300, 60,0,"交割开始日: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999203", 0, OX+300, 75,0,"交割结束日: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999204", 0, OX+300, 90,0,"交易日: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999206", 0, OX+300, 110,0,"交易时段1: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999207", 0, OX+300, 125,0,"交易时段1: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999208", 0, OX+300, 145,0,"交易时段2: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999209", 0, OX+300, 160,0,"交易时段2: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999210", 0, OX+300, 180,0,"交易时段3: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999211", 0, OX+300, 195,0,"交易时段3: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999212", 0, OX+300, 215,0,"交易时段4: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999213", 0, OX+300, 230,0,"交易时段4: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999301", 0, OX+500, 45,0,"卖五价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999302", 0, OX+500, 60,0,"卖四: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999303", 0, OX+500, 75,0,"卖三价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999304", 0, OX+500, 90,0,"卖二价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999305", 0, OX+500, 105,0,"卖一价: ", "微软雅黑",10,clrLightGreen);

   EA999LABEL("EA999306", 0, OX+500, 125,0,"买一价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999307", 0, OX+500, 140,0,"买二价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999308", 0, OX+500, 155,0,"买三价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999309", 0, OX+500, 170,0,"买四价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999310", 0, OX+500, 185,0,"买五价: ", "微软雅黑",10,clrLightGreen);
   
   EA999LABEL("EA999320", 0, OX+500, 210,0,"最新价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999321", 0, OX+500, 225,0,"昨结算价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999322", 0, OX+500, 240,0,"昨收盘价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999323", 0, OX+500, 255,0,"今开盘价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999324", 0, OX+500, 270,0,"今最高价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999325", 0, OX+500, 285,0,"今最低价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999326", 0, OX+500, 300,0,"今收盘价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999327", 0, OX+500, 315,0,"今结算价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999328", 0, OX+500, 330,0,"涨停板价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999329", 0, OX+500, 345,0,"跌停板价: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999330", 0, OX+500, 360,0,"今日均价: ", "微软雅黑",10,clrLightGreen);


   EA999LABEL("EA999401", 0, OX+700, 45,0,"成交金额: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999402", 0, OX+700, 60,0,"昨持仓量/今持仓量: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999403", 0, OX+700, 75,0,"开仓手续费率: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999404", 0, OX+700, 90,0,"开仓手续费: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999405", 0, OX+700, 105,0,"平仓手续费率: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999406", 0, OX+700, 120,0,"开平仓手续费: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999407", 0, OX+700, 135,0,"平今仓手续费: ", "微软雅黑",10,clrLightGreen);
   
   EA999LABEL("EA999410", 0, OX+700, 160,0,"杠杆比率为1计算可报单数量: ", "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999411", 0, OX+700, 174,0,"30%计算可报单数量: ", "微软雅黑",10,clrLightGreen);
   
   EA999LABEL("EA999501", 0, OX+900, 45,0,"登陆账户ID: " + account.Login(), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999502", 0, OX+900, 60,0,"经纪商ID: " + account.BrokerID(), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999503", 0, OX+900, 75,0,"登陆时间: " + account.LoginTime(), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999504", 0, OX+900, 90,0,"交易系统名称: " + account.SystemName(), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999505", 0, OX+900, 105,0,"最大报单引用: " + DoubleToString(account.MaxOrderRef(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999506", 0, OX+900, 120,0,"账户类型: " + CharToString(account.AccountType()) + " : "+ account.AccountTypeDescription(), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999507", 0, OX+900, 135,0,"昨结算权益: " + DoubleToString(account.PreBalance(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999508", 0, OX+900, 150,0,"昨保证金占用: " + DoubleToString(account.PreMargin(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999509", 0, OX+900, 165,0,"动态权益: " + DoubleToString(account.Balance(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999510", 0, OX+900, 180,0,"可用资金: " + DoubleToString(account.Available(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999511", 0, OX+900, 195,0,"保证金占用: " + DoubleToString(account.Margin(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999512", 0, OX+900, 210,0,"手续费: " + DoubleToString(account.Commission(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999513", 0, OX+900, 225,0,"交割保证金: " + DoubleToString(account.DeliveryMargin(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999514", 0, OX+900, 240,0,"入金: " + DoubleToString(account.Deposit(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999515", 0, OX+900, 255,0,"出金: " + DoubleToString(account.Withdraw(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999516", 0, OX+900, 270,0,"保证金冻结: " + DoubleToString(account.FrozenMargin(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999517", 0, OX+900, 285,0,"手续费冻结: " + DoubleToString(account.FrozenCommission(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999518", 0, OX+900, 300,0,"平仓盈亏: " + DoubleToString(account.CloseProfit(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999519", 0, OX+900, 315,0,"持仓盈亏: " + DoubleToString(account.PositionProfit(),2), "微软雅黑",10,clrLightGreen);
   EA999LABEL("EA999520", 0, OX+900, 330,0,"是否登陆: " + BoolToChinese(account.AccountExists()), "微软雅黑",10,clrLightGreen);
   
   EA999LABEL("EA999temp", 0, OX+500, 15,0,"临时信息: 仅用于临时检查某个值 ", "微软雅黑",10,clrLightGreen);
   // 界面重绘
   ChartRedraw();   
   return(INIT_SUCCEEDED);
  }

void OnDeinit(const int reason)
  {
//--- destroy timer
   EventKillTimer();
//---
   ObjectsDeleteAll(0);
   // 界面重绘
   ChartRedraw();
  }

void OnTick()
  {
   //--检查用户登陆状态|合约可交易状态
   if(!symbol.SymbolExists())
   {
      return;
   }   
   //下面代码用于获得商品相关信息
   string SymName = symbol.Name();              //合约名称
   string SymCode = symbol.Symbol();            //合约代码
   string SymExCode = symbol.ExchangeID();      //交易所代码
   string SymExIID = symbol.ExchangeInstID();   //合约在交易所的代码
   string SymProductID = symbol.ProductID();    //产品代码
   string SymProductName = symbol.ProductName();   //产品名称

   string SymExchangeProductID = symbol.ExchangeProductID();   //交易所产品代码
   string SymUnderlyingInstrID = symbol.UnderlyingInstrID();   //基础商品代码

   string SymCurrency = symbol.Currency();               //交易币种
   string SymCreateDate = symbol.CreateDate();           //创建日
   string SymOpenDate = symbol.OpenDate();               //上市日
   string SymExpireDate = symbol.ExpireDate();           //到期日
   string SymStartDelivDate = symbol.StartDelivDate();      //开始交割日
   string SymEndDelivDate = symbol.EndDelivDate();          //结束交割日
   string SymEnterTime = symbol.EnterTime();                //进入本状态时间
   string SymTradingDay = symbol.TradingDay();              //交易日(行情)
   string SymActionDay = symbol.ActionDay();                //业务日期(行情)
   string SymUpdateTime = symbol.UpdateTime();              //最后更新时间(行情)
//string SymSessionStart = symbol.SessionStart(1);          //交易时段开始时间
//string SymSessionEnd = symbol.SessionEnd(1);              //交易时段结束时间

   double SymPriceTick = symbol.PriceTick();                //最小变动价位

   double SymUnderlyingMultiple = symbol.UnderlyingMultiple(); //合约基础商品乘数
   double SymStrikePrice = symbol.StrikePrice();               //执行价 (无效)
   double SymLastPrice = symbol.LastPrice();                   //最新价
   double SymPreSettlement = symbol.PreSettlement();           //昨结算价
   double SymPreClose = symbol.PreClose();                     //昨收盘
   double SymOpen = symbol.Open();                             //今开盘
   double SymHigh = symbol.High();                             //今最高价
   double SymLow = symbol.Low();                               //今最低价
   double SymClose = symbol.Close();                             //今收盘价
   double SymSettlement = symbol.Settlement();                 //今结算价
   double SymUpperLimit = symbol.UpperLimit();                 //涨停板价
   double SymLowerLimit = symbol.LowerLimit();                 //跌停板价


   double SymBid = symbol.Bid();                //申买价一
   double SymBid2 = symbol.Bid2();                //申买价二
   double SymBid3 = symbol.Bid3();                //申买价三
   double SymBid4 = symbol.Bid4();                //申买价四
   double SymBid5 = symbol.Bid5();                //申买价五

   double SymAsk = symbol.Ask();                //申卖价一
   double SymAsk2 = symbol.Ask2();                //申卖价二
   double SymAsk3 = symbol.Ask3();                //申卖价三
   double SymAsk4 = symbol.Ask4();                //申卖价四
   double SymAsk5 = symbol.Ask5();                //申卖价五


   double SymAvgPrice = symbol.AvgPrice();      //当日均价
   double SymTurnover = symbol.Turnover();      //成交金额
   double SymPreOpenInterest = symbol.OpenInterest();        //昨持仓量
   double SymOpenInterest = symbol.OpenInterest();          //今持仓量

   double SymLongMarginRatioByMoney = symbol.LongMarginRatioByMoney();  //多头保证金率 (仅限期权)
   double SymLongMarginRatioByVolume = symbol.LongMarginRatioByVolume();   //多头保证金费 (仅限期权)

   double SymShortMarginRatioByMoney = symbol.ShortMarginRatioByMoney();   //空头保证金率 (仅限期权)
   double SymShortMarginRatioByVolume = symbol.ShortMarginRatioByVolume();         //空头保证金费 (仅限期权)

   double SymExchangeLongMarginRatioByMoney = symbol.ExchangeLongMarginRatioByMoney(); //交易所多头保证金率

   double SymExchangeLongMarginRatioByVolume = symbol.ExchangeLongMarginRatioByVolume(); //交易所多头保证金费

   double SymExchangeShortMarginRatioByMoney = symbol.ExchangeShortMarginRatioByMoney();   //交易所空头保证金率

   double SymExchangeShortMarginRatioByVolume = symbol.ExchangeShortMarginRatioByVolume(); //交易所空头保证金费

   double SymOpenRatioByMoney = symbol.OpenRatioByMoney();      //开仓手续费率

   double SymOpenRatioByVolume = symbol.OpenRatioByVolume();    //开仓手续费

   double SymCloseRatioByMoney = symbol.CloseRatioByMoney();       //平仓手续费率

   double SymCloseRatioByVolume = symbol.CloseRatioByVolume();     //平仓手续费

   double SymCloseTodayRatioByMoney = symbol.CloseTodayRatioByMoney();       //平今仓手续费率

   double SymCloseTodayRatioByVolume = symbol.CloseTodayRatioByVolume();    //平今仓手续费

   double SymOrderCommByVolume  = symbol.OrderCommByVolume();            //报单手续费

   double SymOrderActionCommByVolume = symbol.OrderActionCommByVolume();     //撤单手续费


   char  SymProductClass = symbol.ProductClass();              //产品类型
   string   SymProductClassDescription = symbol.ProductClassDescription();
   long  SymDeliveryYear = symbol.DeliveryYear();              //交割年份
   long  SymDeliveryMonth = symbol.DeliveryMonth();            //交割月份
   long  SymMaxMarketOrderVolume = symbol.MaxMarketOrderVolume();    //市价最大下单量
   long  SymMinMarketOrderVolume = symbol.MinMarketOrderVolume();    //市价最小下单量
   long  SymMaxLimitOrderVolume = symbol.MaxLimitOrderVolume();      //限价最大下单量
   long  SymMinLimitOrderVolume = symbol.MinLimitOrderVolume();       //限价最小下单量

   long  SymContractSize = symbol.ContractSize();                     //合约数量/乘数

   int   SymDigits = (int)symbol.Digits();                                 //小数点位数

   char  SymInstLifePhase = symbol.InstLifePhase();                   //合约生命周期状态
   string   SymInstLifePhaseDescription = symbol.InstLifePhaseDescription();

   bool  SymIsTrading = symbol.IsTrading();                             //当前是否交易

   char  SymPositionType = symbol.PositionType();                       //持仓类型
   string   SymPositionTypeDescription = symbol.PositionTypeDescription();

   char  SymPositionDateType= symbol.PositionDateType();                //持仓日期类型
   string SymPositionDateTypeDescription = symbol.PositionDateTypeDescription();

   char SymCloseDealType = symbol.CloseDealType();                     //平仓处理类型
   string SymCloseDealTypeDescription = symbol.CloseDealTypeDescription();

   char SymMortgageFundUseRange = symbol.MortgageFundUseRange();       //质押资金可用范围
   string SymMortgageFundUseRangeDescription = symbol.MortgageFundUseRangeDescription();

   char SymMaxMarginSideAlgorithm = symbol.MaxMarginSideAlgorithm();    //是否使用大额单边保证金算法
   string SymMaxMarginSideAlgorithmDescription = symbol.MaxMarginSideAlgorithmDescription();

   char SymOptionsType = symbol.OptionsType();                            //期权类型
   string SymOptionsTypeDescription = symbol.OptionsTypeDescription();

   char SymCombinationType = symbol.CombinationType();                    //组合类型
   string SymCombinationTypeDescription = symbol.CombinationTypeDescription();

   bool SymIsIndex  = symbol.IsIndex();                                   //是否指数合约
   bool SymIsMain = symbol.IsMain();                                      //是否主力合约
   bool SymIsSubMarket = symbol.IsSubMarket();                            //是否已订阅行情
   bool SymSymbolExists = symbol.SymbolExists();                           //是否交易时间

   char SymStatus = symbol.Status();                                       //合约交易状态
   string SymStatusDescription = symbol.StatusDescription();

   char SymEnterReason = symbol.EnterReason();                             //进入本状态原因
   string SymEnterReasonDescription = symbol.EnterReasonDescription();

   long SymVolume = symbol.Volume();                                       //成交数量（行情）
   long SymUpdateMillisec = symbol.UpdateMillisec();    //最后修改毫秒（行情）
   long SymBidVolume  = symbol.BidVolume();             //申买量一（行情）
   long SymAskVolume = symbol.AskVolume();              //申卖量一（行情）
   long SymBid2Volume = symbol.Bid2Volume();            //申买量二（行情）

   long SymAsk2Volume = symbol.Bid2Volume();            //申卖量二（行情）

   long SymBid3Volume = symbol.Bid3Volume();            //申买量三（行情）

   long SymAsk3Volume = symbol.Ask3Volume();            //申卖量三（行情）

   long SymBid4Volume = symbol.Bid4Volume();            //申买量四（行情）

   long SymAsk4Volume = symbol.Ask4Volume();            //申卖量四（行情）

   long SymBid5Volume = symbol.Bid5Volume();            //申买量五（行情）

   long SymAsk5Volume = symbol.Ask5Volume();            //申卖量五（行情）

   int SymSessionsTotal = symbol.SessionsTotal();         //交易时段数

   datetime SymSessionStartTime = symbol.SessionStartTime(1);    //交易时段开始时间/本地时间

   datetime SymSessionEndTime = symbol.SessionEndTime(1);        //交易时段结束时间/本地时间


   //下面代码将商品信息变量显示在标签内
   ObjectSetString(0,"EA999101",OBJPROP_TEXT,"商品名称: " + SymName);
   ObjectSetString(0,"EA999102",OBJPROP_TEXT,"合约代码: " + SymCode);
   ObjectSetString(0,"EA999103",OBJPROP_TEXT,"交易所代码: " + SymExCode);
   ObjectSetString(0,"EA999104",OBJPROP_TEXT,"合约在交易所的代码: " + SymExIID);
   ObjectSetString(0,"EA999105",OBJPROP_TEXT,"产品代码: " + SymProductID);
//ObjectSetString(0,"EA999106",OBJPROP_TEXT,"产品名称: " + SymProductName);
//ObjectSetString(0,"EA999107",OBJPROP_TEXT,"交易所产品代码: " + SymExchangeProductID);
//ObjectSetString(0,"EA999108",OBJPROP_TEXT,"基础商品代码: " + SymUnderlyingInstrID);

   ObjectSetString(0,"EA999106",OBJPROP_TEXT,"是否主力合约: " + BoolToChinese(SymIsMain));
   ObjectSetString(0,"EA999107",OBJPROP_TEXT,"是否订阅行情: " + BoolToChinese(SymIsSubMarket));
   ObjectSetString(0,"EA999108",OBJPROP_TEXT,"是否交易时间: " + BoolToChinese(SymSymbolExists));


   ObjectSetString(0,"EA999109",OBJPROP_TEXT,"多头保证金率: " + DoubleToString(SymLongMarginRatioByMoney,5));
//ObjectSetString(0,"EA999110",OBJPROP_TEXT,"多头保证金费: " + DoubleToString(SymLongMarginRatioByVolume,2));
   ObjectSetString(0,"EA999110",OBJPROP_TEXT,"多头保证金费: " + DoubleToString(SymLongMarginRatioByMoney * SymContractSize * SymAsk,2));

   ObjectSetString(0,"EA999111",OBJPROP_TEXT,"空头保证金率: " + DoubleToString(SymShortMarginRatioByMoney,5));
   ObjectSetString(0,"EA999112",OBJPROP_TEXT,"空头保证金费: " + DoubleToString(SymShortMarginRatioByMoney * SymContractSize * SymBid,2));

   ObjectSetString(0,"EA999113",OBJPROP_TEXT,"合约大小: " + DoubleToString(SymContractSize,0));
   ObjectSetString(0,"EA999113",OBJPROP_TEXT,"合约基础商品乘数: " + DoubleToString(SymUnderlyingMultiple,2));

   ObjectSetString(0,"EA999115",OBJPROP_TEXT,"市价/限价最大单量: " + DoubleToString(SymMaxMarketOrderVolume,0) + " / " + DoubleToString(SymMaxLimitOrderVolume,0));
   ObjectSetString(0,"EA999116",OBJPROP_TEXT,"市价/限价最小单量: " + DoubleToString(SymMinMarketOrderVolume,0) + " / " + DoubleToString(SymMinLimitOrderVolume,0));

   ObjectSetString(0,"EA999117",OBJPROP_TEXT,"最小价格跳动: "  + DoubleToString(SymPriceTick,SymDigits));
   
//ObjectSetString(0,"EA999temp",OBJPROP_TEXT,"最新价: " + DoubleToString(SymStrikePrice,2));
//ObjectSetString(0,"EA999temp",OBJPROP_TEXT,"持仓类型: " + SymPositionType +" : " +  SymPositionTypeDescription);
   ObjectSetString(0,"EA999temp",OBJPROP_TEXT,"合约状态: " + CharToString(SymStatus) +" : " +  SymStatusDescription);


   ObjectSetString(0,"EA999201",OBJPROP_TEXT,"到期日: " + SymExpireDate);
   ObjectSetString(0,"EA999202",OBJPROP_TEXT,"交割开始日: " + SymStartDelivDate);
   ObjectSetString(0,"EA999203",OBJPROP_TEXT,"交割结束日: " + SymStartDelivDate);
   ObjectSetString(0,"EA999204",OBJPROP_TEXT,"交易日: " + SymTradingDay);

   ObjectSetString(0,"EA999206",OBJPROP_TEXT,"交易时段1: " + symbol.SessionStart(0));
   ObjectSetString(0,"EA999207",OBJPROP_TEXT,"交易时段1: " + symbol.SessionEnd(0));

   ObjectSetString(0,"EA999208",OBJPROP_TEXT,"交易时段2: " + symbol.SessionStart(1));
   ObjectSetString(0,"EA999209",OBJPROP_TEXT,"交易时段2: " + symbol.SessionEnd(1));

   ObjectSetString(0,"EA999210",OBJPROP_TEXT,"交易时段3: " + symbol.SessionStart(2));
   ObjectSetString(0,"EA999211",OBJPROP_TEXT,"交易时段3: " + symbol.SessionEnd(2));

   ObjectSetString(0,"EA999212",OBJPROP_TEXT,"交易时段4: " + symbol.SessionStart(3));
   ObjectSetString(0,"EA999213",OBJPROP_TEXT,"交易时段4: " + symbol.SessionEnd(3));

   ObjectSetString(0,"EA999301",OBJPROP_TEXT,"卖五价: " + DoubleToString(SymAsk5,SymDigits) + " 卖五量: " + DoubleToString(SymAsk5Volume,0));
   ObjectSetString(0,"EA999302",OBJPROP_TEXT,"卖四价: " + DoubleToString(SymAsk4,SymDigits) + " 卖四量: " + DoubleToString(SymAsk4Volume,0));
   ObjectSetString(0,"EA999303",OBJPROP_TEXT,"卖三价: " + DoubleToString(SymAsk3,SymDigits) + " 卖三量: " + DoubleToString(SymAsk3Volume,0));
   ObjectSetString(0,"EA999304",OBJPROP_TEXT,"卖二价: " + DoubleToString(SymAsk2,SymDigits) + " 卖二量: " + DoubleToString(SymAsk2Volume,0));
   ObjectSetString(0,"EA999305",OBJPROP_TEXT,"卖一价: " + DoubleToString(SymAsk,SymDigits) + " 卖一量: " + DoubleToString(SymAskVolume,0));

   ObjectSetString(0,"EA999306",OBJPROP_TEXT,"买一价: " + DoubleToString(SymBid,SymDigits) + " 买一量: " + DoubleToString(SymBidVolume,0));
   ObjectSetString(0,"EA999307",OBJPROP_TEXT,"买二价: " + DoubleToString(SymBid2,SymDigits) + " 买二量: " + DoubleToString(SymBid2Volume,0));
   ObjectSetString(0,"EA999308",OBJPROP_TEXT,"买三价: " + DoubleToString(SymBid3,SymDigits) + " 买三量: " + DoubleToString(SymBid3Volume,0));
   ObjectSetString(0,"EA999309",OBJPROP_TEXT,"买四价: " + DoubleToString(SymBid4,SymDigits) + " 买四量: " + DoubleToString(SymBid4Volume,0));
   ObjectSetString(0,"EA999310",OBJPROP_TEXT,"买五价: " + DoubleToString(SymBid5,SymDigits) + " 买五量: " + DoubleToString(SymBid5Volume,0));

   ObjectSetString(0,"EA999320",OBJPROP_TEXT,"最新价: " + DoubleToString(SymLastPrice,SymDigits));
   ObjectSetString(0,"EA999321",OBJPROP_TEXT,"昨结算价: " + DoubleToString(SymPreSettlement,SymDigits));
   ObjectSetString(0,"EA999322",OBJPROP_TEXT,"昨收盘价: " + DoubleToString(SymPreClose,SymDigits));
   ObjectSetString(0,"EA999323",OBJPROP_TEXT,"今开盘价: " + DoubleToString(SymOpen,SymDigits));
   ObjectSetString(0,"EA999324",OBJPROP_TEXT,"今最高价: " + DoubleToString(SymHigh,SymDigits));
   ObjectSetString(0,"EA999325",OBJPROP_TEXT,"今最低价: " + DoubleToString(SymLow,SymDigits));
   ObjectSetString(0,"EA999326",OBJPROP_TEXT,"今收盘价: " + DoubleToString(SymClose,SymDigits));
   ObjectSetString(0,"EA999327",OBJPROP_TEXT,"今结算价: " + DoubleToString(SymSettlement,SymDigits));
   ObjectSetString(0,"EA999328",OBJPROP_TEXT,"涨停板价: " + DoubleToString(SymUpperLimit,SymDigits)); 
   ObjectSetString(0,"EA999329",OBJPROP_TEXT,"跌停板价: " + DoubleToString(SymLowerLimit,SymDigits));
   ObjectSetString(0,"EA999330",OBJPROP_TEXT,"今日均价: " + DoubleToString(SymAvgPrice,SymDigits));
   
   ObjectSetString(0,"EA999401",OBJPROP_TEXT,"成交金额: " + DoubleToString(SymTurnover,2));
   ObjectSetString(0,"EA999402",OBJPROP_TEXT,"昨/今持仓量: " + DoubleToString(SymPreOpenInterest,0) + " / " + DoubleToString(SymOpenInterest,0));
   ObjectSetString(0,"EA999403",OBJPROP_TEXT,"开仓手续费率: " + DoubleToString(SymOpenRatioByVolume,2));
   ObjectSetString(0,"EA999404",OBJPROP_TEXT,"开仓手续费: " + DoubleToString(SymOpenRatioByMoney,2));
   ObjectSetString(0,"EA999405",OBJPROP_TEXT,"平仓手续费率: " + DoubleToString(SymCloseRatioByVolume,2));
   ObjectSetString(0,"EA999406",OBJPROP_TEXT,"平仓手续费: " + DoubleToString(SymCloseRatioByMoney,2));
   ObjectSetString(0,"EA999407",OBJPROP_TEXT,"平今仓手续费: " + DoubleToString(SymCloseTodayRatioByVolume,2));  
   
   double order_lots_value = ::MathFloor(account.Balance()*1/(symbol.Ask()*symbol.ContractSize())+0.5);
   ObjectSetString(0,"EA999410",OBJPROP_TEXT,"杠杆比率可报单数量: " + DoubleToString(order_lots_value,0));  
   double order_lots_maigin = ::MathFloor(account.Available()*0.3/(symbol.Ask()*symbol.ContractSize()*symbol.LongMarginRatioByMoney())+0.5);
   ObjectSetString(0,"EA999411",OBJPROP_TEXT,"30%计算可报单数量: " + DoubleToString(order_lots_maigin,0)); 
   
   if(!account.AccountExists())
   {
      return;
   }
   ObjectSetString(0,"EA999501",OBJPROP_TEXT,"登陆账户ID: " + account.Login());
   ObjectSetString(0,"EA999502",OBJPROP_TEXT,"经纪商ID: " + account.BrokerID());
   ObjectSetString(0,"EA999503",OBJPROP_TEXT,"登陆时间: " + account.LoginTime());
   ObjectSetString(0,"EA999504",OBJPROP_TEXT,"交易系统名称: " + account.SystemName());
   ObjectSetString(0,"EA999505",OBJPROP_TEXT,"最大报单引用: " + DoubleToString(account.MaxOrderRef(),2));
   ObjectSetString(0,"EA999506",OBJPROP_TEXT,"账户类型: " + CharToString(account.AccountType()) + " : "+ account.AccountTypeDescription());
   ObjectSetString(0,"EA999507",OBJPROP_TEXT,"昨结算权益: " + DoubleToString(account.PreBalance(),2));
   ObjectSetString(0,"EA999508",OBJPROP_TEXT,"昨保证金占用: " + DoubleToString(account.PreMargin(),2));
   ObjectSetString(0,"EA999509",OBJPROP_TEXT,"动态权益: " + DoubleToString(account.Balance(),2));
   ObjectSetString(0,"EA999510",OBJPROP_TEXT,"可用资金: " + DoubleToString(account.Available(),2));
   ObjectSetString(0,"EA999511",OBJPROP_TEXT,"保证金占用: " + DoubleToString(account.Margin(),2));
   ObjectSetString(0,"EA999512",OBJPROP_TEXT,"手续费: " + DoubleToString(account.Commission(),2));
   ObjectSetString(0,"EA999513",OBJPROP_TEXT,"交割保证金: " + DoubleToString(account.DeliveryMargin(),2));
   ObjectSetString(0,"EA999514",OBJPROP_TEXT,"入金: " + DoubleToString(account.Deposit(),2));
   ObjectSetString(0,"EA999515",OBJPROP_TEXT,"出金: " + DoubleToString(account.Withdraw(),2));
   ObjectSetString(0,"EA999516",OBJPROP_TEXT,"保证金冻结: " + DoubleToString(account.FrozenMargin(),2));
   ObjectSetString(0,"EA999517",OBJPROP_TEXT,"手续费冻结: " + DoubleToString(account.FrozenCommission(),2));
   ObjectSetString(0,"EA999518",OBJPROP_TEXT,"平仓盈亏: " + DoubleToString(account.CloseProfit(),2));
   ObjectSetString(0,"EA999519",OBJPROP_TEXT,"持仓盈亏: " + DoubleToString(account.PositionProfit(),2));
   ObjectSetString(0,"EA999520",OBJPROP_TEXT,"是否登陆: " + BoolToChinese(account.AccountExists()));
   
   // 界面重绘
   ChartRedraw();
  }
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
  {
//---
   OnTick();
  }
//+------------------------------------------------------------------+
//| Trade function                                                   |
//+------------------------------------------------------------------+
void OnTrade()
  {
//---

  }
//+------------------------------------------------------------------+
//| TradeTransaction function                                        |
//+------------------------------------------------------------------+
void OnTradeTransaction(const MqlTradeTransaction& trans,
                        const MqlTradeRequest& request,
                        const MqlTradeResult& result)
  {
//---

  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---

  }
//+------------------------------------------------------------------+




//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void createmenu(string 按钮名称,string 显示文字,int X,int Y,int 按钮长度,int 按钮高度,color 字体颜色,color 背景色,string 字体,int 字体大小,int 位置)
  {
//--- Create a button to send custom events
   ObjectCreate(0,按钮名称,OBJ_BUTTON,0,0,0);
   ObjectSetInteger(0,按钮名称,OBJPROP_COLOR,字体颜色);
   ObjectSetInteger(0,按钮名称,OBJPROP_BGCOLOR,背景色);
   ObjectSetInteger(0,按钮名称,OBJPROP_BORDER_COLOR,背景色);
   ObjectSetInteger(0,按钮名称,OBJPROP_BORDER_TYPE,BORDER_FLAT);
   ObjectSetInteger(0,按钮名称,OBJPROP_CORNER,位置);
   ObjectSetInteger(0,按钮名称,OBJPROP_XDISTANCE,X);
   ObjectSetInteger(0,按钮名称,OBJPROP_YDISTANCE,Y);
   ObjectSetInteger(0,按钮名称,OBJPROP_XSIZE,按钮长度);
   ObjectSetInteger(0,按钮名称,OBJPROP_YSIZE,按钮高度);
   ObjectSetString(0,按钮名称,OBJPROP_FONT,字体);
   ObjectSetString(0,按钮名称,OBJPROP_TEXT,显示文字);
   ObjectSetInteger(0,按钮名称,OBJPROP_FONTSIZE,字体大小);
   ObjectSetInteger(0,按钮名称,OBJPROP_SELECTABLE,0);
   ObjectSetInteger(0,按钮名称,OBJPROP_HIDDEN,true);
   ObjectSetInteger(0,按钮名称,OBJPROP_ZORDER,0);
   return;
  }

//+------------------------------------------------------------------+
//|用于生成文字标签
//+------------------------------------------------------------------+
bool EA999LABEL(
   //const long              chart_ID=0,               // 图表 ID
   const string            name="Label",             // 标签名称
   const int               sub_window=0,             // 子窗口指数
   const int               x=0,                      // X 坐标
   const int               y=0,                      // Y 坐标
   const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // 图表定位角
   const string            text="Label",             // 文本
   const string            font="Arial",             // 字体
   const int               font_size=10,             // 字体大小
   const color             clr=clrRed               // 颜色
                               //const double            angle=0.0,                // 文本倾斜
                               //const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_UPPER, // 定位类型
                               //const bool              back=false,               // 在背景中
                               //const bool              selection=false,          // 突出移动
                               //const bool              hidden=true,              // 隐藏在对象列表
                               //const long              z_order=0                 // 鼠标单击优先
)
  {

//--- 重置错误的值
   ResetLastError();
//--- 创建文本标签
   long              chart_ID=0;
   if(!ObjectCreate(chart_ID,name,OBJ_LABEL,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create text label! Error code = ",GetLastError());
      return(false);
     }

   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);              //--- 设置标签坐标
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);            //--- 设置相对于定义点坐标的图表的角
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text);                 //--- 设置文本
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font);                 //--- 设置文本字体
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size);       //--- 设置字体大小
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,0.0);               //--- 设置文本的倾斜角
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,ANCHOR_LEFT_UPPER);            //--- 设置定位类型
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);                //--- 设置颜色
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,false);                //--- 显示前景 (false) 或背景 (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,false);     //--- 启用 (true) 或禁用 (false) 通过鼠标移动标签的模式
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,true);            //--- 在对象列表隐藏(true) 或显示 (false) 图形对象名称
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,0);           //--- 设置在图表中优先接收鼠标点击事件

   return(true);
  }


//+------------------------------------------------------------------+
//|用于生成横线
//+------------------------------------------------------------------+
bool EA999_HLine(
   //const long            chart_ID=0,        // 图表 ID
   const string          name="HLine",      // 线的名称
   const int             sub_window=0,      // 子窗口指数
   double                price=0,           // 线的价格
   const color           clr=clrRed,        // 线的颜色
   const ENUM_LINE_STYLE style=STYLE_SOLID, // 线的风格
   const int             width=1           // 线的宽度
                               //const bool            back=false,        // 在背景中
                               //const bool            selection=true,    // 突出移动
                               //const bool            hidden=true,       // 隐藏在对象列表
                               //const long            z_order=0         // 鼠标单击优先
)
  {
   long            chart_ID=0;
//--- 如果没有设置价格，则在当前卖价水平设置它
   if(!price)
      price=SymbolInfoDouble(Symbol(),SYMBOL_BID);
//--- 重置错误的值
   ResetLastError();
//--- 创建水平线
   if(!ObjectCreate(chart_ID,name,OBJ_HLINE,sub_window,0,price))
     {
      Print(__FUNCTION__,
            ": failed to create a horizontal line! Error code = ",GetLastError());
      return(false);
     }

   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr); //--- 设置线的颜色
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style); //--- 设置线的显示风格
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,width); //--- 设置线的宽度
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,false); //--- 显示前景 (false) 或背景 (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,true); //--- 在对象列表隐藏(true) 或显示 (false) 图形对象名称
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,0); //--- 设置在图表中优先接收鼠标点击事件
   return(true);
  }



//+------------------------------------------------------------------+
//|用于生成方框
//+------------------------------------------------------------------+
bool EA999_RectLabel(
   //const long             chart_ID=0,               // 图表 ID
   const string           name="RectLabel",         // 标签名称
   const int              sub_window=0,             // 子窗口指数
   const int              x=0,                      // X 坐标
   const int              y=0,                      // Y 坐标
   const int              width=50,                 // 宽度
   const int              height=18,                // 高度
   const color            back_clr=C'236,233,216',  // 背景色
   const ENUM_BORDER_TYPE border=BORDER_SUNKEN,     // 边框类型
   const ENUM_BASE_CORNER corner=CORNER_LEFT_UPPER, // 图表定位角
   const color            clr=clrRed,               // 平面边框颜色 (Flat)
   const ENUM_LINE_STYLE  style=STYLE_SOLID,        // 平面边框风格
   const int              line_width=1            // 平面边框宽度
                                     //const bool             back=false,               // 在背景中
                                     //const bool             selection=false,          // 突出移动
                                     //const bool             hidden=true,              // 隐藏在对象列表
                                     //const long             z_order=0)                // 鼠标单击优先
)
  {
   long             chart_ID=0;
//--- 重置错误的值
   ResetLastError();
//--- 创建矩形标签
   if(!ObjectCreate(chart_ID,name,OBJ_RECTANGLE_LABEL,sub_window,0,0))
     {
      Print(__FUNCTION__,
            ": failed to create a rectangle label! Error code = ",GetLastError());
      return(false);
     }

   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x);              //--- 设置标签坐标
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y);
   ObjectSetInteger(chart_ID,name,OBJPROP_XSIZE,width);              //--- 设置标签大小
   ObjectSetInteger(chart_ID,name,OBJPROP_YSIZE,height);
   ObjectSetInteger(chart_ID,name,OBJPROP_BGCOLOR,back_clr);         //--- 设置背景颜色
   ObjectSetInteger(chart_ID,name,OBJPROP_BORDER_TYPE,border);       //--- 设置边框类型
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner);            //--- 设置相对于定义点坐标的图表的角
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr);                //--- 设置平面边框颜色 (在平面模式下)
   ObjectSetInteger(chart_ID,name,OBJPROP_STYLE,style);              //--- 设置平面边框线型风格
   ObjectSetInteger(chart_ID,name,OBJPROP_WIDTH,line_width);         //--- 设置平面边框宽度
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,false);                //--- 显示前景 (false) 或背景 (true)
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,false);         //--- 启用 (true) 或禁用 (false) 通过鼠标移动标签的模式
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,false);
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,true);              //--- 在对象列表隐藏(true) 或显示 (false) 图形对象名称
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,0);                 //--- 设置在图表中优先接收鼠标点击事件
   return(true);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|将BOOL值转为文字，方便显示
//+------------------------------------------------------------------+
string BoolToChinese(bool temp)
  {
   if(temp == false)
     {
      return("否");
     }
   else
     {
      if(temp == true)
        {
         return("是");
        }
        else
        {
        return("未知");
        }
     }


  }
//+------------------------------------------------------------------+
