//+------------------------------------------------------------------+
//|                                                     MACD测试.mq5 |
//+------------------------------------------------------------------+
// 实盘状态：定义 __MT5CTP__
// 测试状态：注释 __MT5CTP__
#define __MT5CTP__
// 包含库
#ifdef __MT5CTP__
#include <mt5ctp\mt5toctp.mqh>
#endif 
//+------------------------------------------------------------------+
//| Expert 参数                                                      |
//+------------------------------------------------------------------+
sinput ulong MagicNumber = 123456;
input double OrderLots = 1;
//+------------------------------------------------------------------+
//| Expert 全局变量                                                  |
//+------------------------------------------------------------------+
// MACD指标
int _handle_macd = INVALID_HANDLE;
// 开仓记录k线时间
datetime _last_order_open_time = 0;
// MACD指标数据
double _singal_buffer[];
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- 初始化指标对象
   if((_handle_macd = iMACD(NULL,0,12,26,9,PRICE_OPEN))==INVALID_HANDLE)
      return(INIT_FAILED);
//--- 设置MACD指标数据
   ArraySetAsSeries(_singal_buffer,true);
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//--- 提取指标数据最新的2个值
   if(_handle_macd==INVALID_HANDLE || CopyBuffer(_handle_macd,1,1,2,_singal_buffer)!=2)
      return;
//---交易条件：
//+------------------------------------------------------------------+
//| MACD向上穿越0轴，平空做多;MACD向下穿越0轴，平多做空
//| 根据MACD的计算规则，实际上是永远在市的双均线策略
//+------------------------------------------------------------------+
   bool cond_buy = _singal_buffer[1]<0 && _singal_buffer[0]>0;
   bool cond_sell = _singal_buffer[1]>0 && _singal_buffer[0]<0;
//-- 平空做多
   if(cond_buy)
     {
      //--- 平空
      for(int i=PositionsTotal()-1; i>=0; i--)
        {
         //-- 方法 PositionGetTicket 取得 ticket
         ulong ticket = PositionGetTicket(i);
         //-- 方法 PositionSelectByTicket 选中一条持仓记录
         if(!PositionSelectByTicket(ticket))
            continue;
         //-- 调用方法检查持仓记录是否符合选择条件
         if(PositionGetInteger(POSITION_MAGIC)==MagicNumber && PositionGetString(POSITION_SYMBOL)==Symbol() && (ENUM_POSITION_TYPE)PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_SELL)
           {
            MqlTradeRequest request= {0};
            MqlTradeResult  result= {0};
            request.action = TRADE_ACTION_DEAL;
            request.symbol = PositionGetString(POSITION_SYMBOL);
            request.type = ORDER_TYPE_BUY;
            request.position = ticket;
            request.price = SymbolInfoDouble(Symbol(),SYMBOL_ASK);
            request.deviation = 5;
            request.volume = PositionGetDouble(POSITION_VOLUME);
            if(OrderSend(request,result))
              {
               _last_order_open_time = 0;
              }
           }
        }
      //--- 开多
      if(_last_order_open_time!=iTime(NULL,0,0))
        {
         MqlTradeRequest request= {0};
         MqlTradeResult  result= {0};
         request.action = TRADE_ACTION_DEAL;
         request.symbol = Symbol();
         request.type = ORDER_TYPE_BUY;
         request.price = SymbolInfoDouble(Symbol(),SYMBOL_ASK);
         request.deviation = 5;
         request.volume = OrderLots;
         request.magic = MagicNumber;
         if(OrderSend(request,result))
            _last_order_open_time = iTime(NULL,0,0);
        }
     }
//-- 平多
   if(cond_sell)
     {
      //--- 平多
      for(int i=PositionsTotal()-1; i>=0; i--)
        {
         //-- 方法 PositionGetTicket 取得 ticket
         ulong ticket = PositionGetTicket(i);
         //-- 方法 PositionSelectByTicket 选中一条持仓记录
         if(!PositionSelectByTicket(ticket))
            continue;
         //-- 调用方法等检查持仓记录是否符合选择条件
         if(PositionGetInteger(POSITION_MAGIC)==MagicNumber && PositionGetString(POSITION_SYMBOL)==Symbol() && (ENUM_POSITION_TYPE)PositionGetInteger(POSITION_TYPE)==POSITION_TYPE_BUY)
           {
            MqlTradeRequest request= {0};
            MqlTradeResult  result= {0};
            request.action = TRADE_ACTION_DEAL;
            request.symbol = PositionGetString(POSITION_SYMBOL);
            request.type = ORDER_TYPE_SELL;
            request.position = ticket;
            request.price = SymbolInfoDouble(Symbol(),SYMBOL_BID);
            request.deviation = 5;
            request.volume = PositionGetDouble(POSITION_VOLUME);
            if(OrderSend(request,result))
              {
               _last_order_open_time = 0;
              }
           }
        }
      //--- 开空
      if(_last_order_open_time!=iTime(NULL,0,0))
        {
         MqlTradeRequest request= {0};
         MqlTradeResult  result= {0};
         request.action = TRADE_ACTION_DEAL;
         request.symbol = Symbol();
         request.type = ORDER_TYPE_SELL;
         request.price = SymbolInfoDouble(Symbol(),SYMBOL_BID);
         request.deviation = 5;
         request.volume = OrderLots;
         request.magic = MagicNumber;
         if(OrderSend(request,result))
            _last_order_open_time = iTime(NULL,0,0);
        }
     }
  }
//+------------------------------------------------------------------+
